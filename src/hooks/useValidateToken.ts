import { loginWithToken } from "@/services/login";
import { useSearchParams } from "next/navigation";
import { toast } from "react-toastify";

export const useValidateToken = () => {
  const token = useSearchParams().get("token");

  const validateToken = async () => {
    if (!token) return;
    const response = await loginWithToken(token);
    const accessToken = response?.accessToken;
    if (!response || !accessToken) {
      toast("Invalid token", { type: "error" });
      return;
    }
    localStorage.setItem("access_token", accessToken);
    return accessToken;
  };

  return { validateToken };
};
