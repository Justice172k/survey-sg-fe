"use client";

import { Modal } from "antd";
import { useState } from "react";

export default function Home() {
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState(
    "To register please login Govaa acount"
  );

  const showModal = () => {
    setOpen(true);
  };

  const handleOk = () => {
    setModalText("The modal will be closed after two seconds");
    setConfirmLoading(true);
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
    }, 2000);

    window.location.href = `${process.env.NEXT_PUBLIC_GOVAA_URL}/login?url_callback=${process.env.NEXT_PUBLIC_SURVEY_SG_FE_URL}/register`;
  };

  const handleCancel = () => {
    setOpen(false);
  };
  const handleLogin = () => {
    window.location.href = `${process.env.NEXT_PUBLIC_GOVAA_URL}/login?url_callback=${process.env.NEXT_PUBLIC_SURVEY_SG_FE_URL}/my-profile`;
  };
  return (
    <div>
      <div className="flex w-full h-screen bg-white items-center justify-center">
        <div className="bg-white p-8 rounded-lg shadow-md w-96">
          <h2 className="text-2xl font-semibold text-center">Login</h2>
          <div className="mt-4">
            <button
              className="w-full py-2 px-4 bg-blue-500 hover:bg-blue-600 text-white rounded focus:outline-none focus:ring focus:ring-blue-300 transition duration-300"
              onClick={handleLogin}
            >
              Login
            </button>
          </div>
          <div className="mt-4">
            <button
              className="w-full py-2 px-4 bg-gray-300 hover:bg-gray-400 text-gray-700 rounded focus:outline-none focus:ring focus:ring-gray-400 transition duration-300"
              onClick={showModal}
            >
              Register
            </button>
          </div>
        </div>

        <Modal
          okButtonProps={{
            style: {
              color: "black",
              borderRadius: "5px",
              border: "1px solid black",
            },
          }}
          okText="Continue"
          title="Title"
          open={open}
          onOk={handleOk}
          confirmLoading={confirmLoading}
          onCancel={handleCancel}
          cancelButtonProps={{
            style: {
              color: "gray",
              borderRadius: "5px",
              border: "1px solid black",
            },
          }}
        >
          <p>{modalText}</p>
        </Modal>
      </div>
    </div>
  );
}
