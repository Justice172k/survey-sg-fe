"use client";

import FinalRegister from "@/components/FinalRegister";
import { useSearchParams } from "next/navigation";

const Register = () => {
  const token = useSearchParams().get("token");
  return (
    <div className="w-full h-screen bg-wh">
      {token && (
        <div>
          <FinalRegister token={token} />
        </div>
      )}
    </div>
  );
};

export default Register;
