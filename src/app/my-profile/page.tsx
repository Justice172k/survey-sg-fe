"use client";
import { useValidateToken } from "@/hooks/useValidateToken";
import { getCurrentUser } from "@/services/get-current-user";
import { User } from "@/types/User";
import { useEffect, useState } from "react";

const MyProfile = () => {
  const [user, setUser] = useState<User | null>(null);
  const { validateToken } = useValidateToken();

  useEffect(() => {
    (async () => {
      const accessToken = await validateToken();
      if (!accessToken) return;
      getUserData(accessToken);
    })();
  }, []);

  const getUserData = async (accessToken: string) => {
    const res = await getCurrentUser(accessToken);
    setUser(res);
  };

  if (!user) return <div>Unauthenticated</div>;

  return (
    <div className="bg-gray-100 h-screen w-full">
      <div className="container mx-auto p-4 h-screen flex items-center">
        <div className="max-w-lg mx-auto bg-white rounded-lg shadow-md w-1/3 ">
          <div className="p-4">
            <h1 className="text-2xl font-bold text-center mt-2">{user.name}</h1>
            <p className="text-gray-600 text-sm text-center">
              Agency Name: {user.agency}
            </p>

            <div className="mt-4">
              <p className="text-gray-700">
                Email:{" "}
                <span className="text-gray-800">
                  {user.email || "your.email@example.com"}
                </span>
              </p>
            </div>
            <div className="mt-4">
              <p className="text-gray-700">Description:</p>
              <p className="text-gray-800">
                {user.description || "description will be displayed here"}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyProfile;
