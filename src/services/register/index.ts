export const register = async (payload: {
  name: string;
  email: string;
  agency: string;
  description: string;
  govEmail: string;
}) => {
  try {
    const res = await fetch("http://localhost:3001/auth/register", {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(payload),
    });
    return await res.json();
  } catch (error) {
    console.log(error);
  }
};
