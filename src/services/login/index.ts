type TokenResponse = { accessToken: string };
export const loginWithToken = async (
  token: string
): Promise<TokenResponse | undefined> => {
  try {
    const res = await fetch("http://localhost:3001/auth/login", {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify({
        token,
      }),
    });
    return await res.json();
  } catch (error) {
    console.log(error);
  }
};
