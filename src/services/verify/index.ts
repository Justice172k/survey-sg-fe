export const verify = async (token: string) => {
  try {
    const response = await fetch(
      "http://localhost:3001/auth/verify-govaa-token",
      {
        method: "POST",
        headers: {
          "content-type": "application/json",
        },
        body: JSON.stringify({
          token,
        }),
      }
    );
    return await response.json();
  } catch (error) {
    console.log(error);
  }
};
