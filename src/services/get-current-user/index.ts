export const getCurrentUser = async (token: string) => {
  try {
    const res = await fetch("http://localhost:3001/user/current-user", {
      method: "GET",
      headers: {
        "content-type": "application/json",
        authorization: `Bearer ${token}`,
      },
    });
    return await res.json();
  } catch (error) {
    console.log(error);
  }
};
