"use client";
import { register } from "@/services/register";
import { verify } from "@/services/verify";
import { User } from "@/types/User";
import { agencies } from "@/utils";
import { Button, Checkbox, Form, Input, Select } from "antd";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

const { Option } = Select;

const initialForm: User = {
  name: "",
  email: "",
  agency: "",
  description: "",
};

const layout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 16 },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const FinalRegister = ({ token }: { token: string }) => {
  const route = useRouter();
  const [email, setEmail] = useState("");
  const [form] = Form.useForm();

  useEffect(() => {
    if (!token) return;
    getFormInitValueByToken(token);
  }, []);

  const onFinish = async () => {
    const payload = {
      name: form.getFieldValue("name"),
      email: form.getFieldValue("email"),
      agency: form.getFieldValue("agency"),
      description: form.getFieldValue("description"),
      govEmail: email,
    };

    const res = await register(payload);
    if (res.status === 409) {
      toast("Email already registered");
      return;
    }
    toast("Registered Successfully");
    route.push("/my-profile?token=" + token);
  };

  const getFormInitValueByToken = async (token: string) => {
    const res = await verify(token);
    const { email } = res.payload;
    setEmail(email);
    form.setFieldsValue(res.payload);
  };

  return (
    <div className="min-h-screen bg-gradient-to-r from-blue-300 to-blue-500 flex items-center justify-center">
      <Form
        className="w-full max-w-2xl p-8 bg-white rounded-lg shadow-md"
        {...layout}
        form={form}
        name="control-hooks"
        onFinish={onFinish}
      >
        <h1 className="text-2xl font-semibold text-center text-gray-800 mb-4">
          Register
        </h1>
        <Form.Item name="name" label="Name" rules={[{ required: true }]}>
          <Input className="w-full border rounded-lg px-3 py-2 text-gray-700 focus:ring focus:ring-blue-300" />
        </Form.Item>
        <Form.Item
          name="email"
          label="Contact Email"
          rules={[{ required: true }]}
        >
          <Input className="w-full border rounded-lg px-3 py-2 text-gray-700 focus:ring focus:ring-blue-300" />
        </Form.Item>
        <Form.Item name="agency" label="Agency" rules={[{ required: true }]}>
          <Select
            placeholder="Select a option and change input text above"
            allowClear
          >
            {agencies.map((agency) => (
              <Option key={agency} value={agency}>
                {agency}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          name="description"
          label="Description"
          rules={[{ required: true }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="agreement"
          valuePropName="checked"
          rules={[
            {
              validator: (_, value) =>
                value
                  ? Promise.resolve()
                  : Promise.reject(new Error("Should accept agreement")),
            },
          ]}
          {...tailFormItemLayout}
        >
          <Checkbox>
            I have read the <a href="">agreement</a>
          </Checkbox>
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button
            type="primary"
            htmlType="submit"
            className="w-1/2 bg-blue-500 hover:bg-blue-600 text-white rounded-lg focus:ring focus:ring-blue-300"
          >
            Register
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default FinalRegister;
