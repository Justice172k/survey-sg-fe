/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  env: {
    NEXT_PUBLIC_GOVAA_URL: "http://localhost:8080",
    NEXT_PUBLIC_SURVEY_SG_FE_URL: "http://localhost:3000",
  },
};

module.exports = nextConfig;
